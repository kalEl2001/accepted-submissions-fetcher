package main

import (
	"bytes"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	_ "github.com/joho/godotenv/autoload"
)

func main() {

	authToken := getAuth()

	var contestJid string
	var modeSelect int32

	fmt.Print(`
         ___  ____   ___     ___  __   _  _  ____  ____  ____  ____  ____     __  ____             
        / __)(  _ \ / __)   / __)/  \ ( \/ )(  _ \(  __)(  __)/ ___)(_  _)   /  \(___ \            
       ( (__  ) __/( (__   ( (__(  O )/ \/ \ ) __/ ) _)  ) _) \___ \  )(    (_/ / / __/            
	\___)(__)   \___)   \___)\__/ \_)(_/(__)  (__)  (____)(____/ (__)    (__)(____)            
 ____  _  _  ____  _  _  __  ____  ____  __  __   __ _  ____    ____  ____  ____  ___  _  _  ____  ____ 
/ ___)/ )( \(  _ \( \/ )(  )/ ___)/ ___)(  )/  \ (  ( \/ ___)  (  __)(  __)(_  _)/ __)/ )( \(  __)(  _ \
\___ \) \/ ( ) _ (/ \/ \ )( \___ \\___ \ )((  O )/    /\___ \   ) _)  ) _)   )( ( (__ ) __ ( ) _)  )   /
(____/\____/(____/\_)(_/(__)(____/(____/(__)\__/ \_)__)(____/  (__)  (____) (__) \___)\_)(_/(____)(__\_)
`)

	fmt.Println("\nINPUT CONTEST JID:")
	fmt.Print("CONTEST JID: ")
	fmt.Scanf("%s", &contestJid)

	fmt.Println("SELECT SUBMISSIONS TO DOWNLOAD:")
	fmt.Println("1. ALL CONTESTANTS' LATEST SUBMISSIONS FOR EACH PROBLEM")
	fmt.Println("2. CONTESTANTS' SUBMISSIONS WITH ACCEPTED VERDICT ONLY")
	fmt.Println("3. CONTESTANTS' LATEST HIGHEST SCORE SUBMISSIONS")
	fmt.Print("SELECT: ")
	fmt.Scanf("%d", &modeSelect)

	if modeSelect != 1 && modeSelect != 2 && modeSelect != 3 {
		panic("SELECTION NOT VALID!")
	}

	submissionsList := getAllSubmissionsList(contestJid, authToken)

	var submissionsID []string

	if modeSelect == 1 {
		submissionsID = getAllSubmissionsID(submissionsList)
	} else if modeSelect == 2 {
		submissionsID = getAllAcceptedSubmissionsID(submissionsList)
	} else if modeSelect == 3 {
		submissionsID = getLatestHighestSubmissionsID(submissionsList)
	}

	processSubmission(submissionsID, authToken)

	fmt.Print(`
    ____  ____  ____  ___  _  _       
   (  __)(  __)(_  _)/ __)/ )( \      
    ) _)  ) _)   )( ( (__ ) __ (      
   (__)  (____) (__) \___)\_)(_/      
 ____  _  _   ___  ___  ____  ____  ____ 
/ ___)/ )( \ / __)/ __)(  __)(  __)(    \
\___ \) \/ (( (__( (__  ) _)  ) _)  ) D (
(____/\____/ \___)\___)(____)(____)(____/
`)

}

func getAuth() string {

	requestBody, err := json.Marshal(map[string]string{
		"usernameOrEmail": os.Getenv("JUDGELS_ADMIN_USERNAME"),
		"password":        os.Getenv("JUDGELS_ADMIN_PASSWORD"),
	})
	catchError(err)

	url := os.Getenv("JOPHIEL_URL") + "/api/v2/session/login"

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(requestBody))
	catchError(err)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	catchError(err)

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	authToken := fmt.Sprintf("%v", result["token"])

	return authToken

}

func getAllSubmissionsList(contestJid string, token string) []interface{} {

	var result []interface{}

	url := os.Getenv("URIEL_URL") + "/api/v2/contests/submissions/programming/?contestJid=" + contestJid + "&page="

	pageNumber := 1

	bearer := "Bearer " + token

	for {
		currentPageURL := url + strconv.Itoa(pageNumber)

		req, err := http.NewRequest("GET", currentPageURL, nil)
		catchError(err)

		req.Header.Add("Authorization", bearer)

		client := &http.Client{}
		resp, err := client.Do(req)
		catchError(err)

		body, err := ioutil.ReadAll(resp.Body)
		catchError(err)

		var currentPageResult map[string]interface{}
		json.Unmarshal([]byte(body), &currentPageResult)

		currentPageData := currentPageResult["data"].(map[string]interface{})["page"].([]interface{})

		numOfSubmissionInPage := len(currentPageResult["data"].(map[string]interface{})["page"].([]interface{}))

		if numOfSubmissionInPage < 1 {
			break
		}

		pageNumber++

		result = append(result, currentPageData...)
	}

	return result

}

func getAllAcceptedSubmissionsID(submissionsList []interface{}) []string {

	var result []string

	for _, page := range submissionsList {

		submissionID := fmt.Sprintf("%d", int(page.(map[string]interface{})["id"].(float64)))
		submissionVerdict := page.(map[string]interface{})["latestGrading"].(map[string]interface{})["verdict"].(map[string]interface{})["code"].(string)

		if submissionVerdict == "AC" {

			result = append(result, submissionID)

		}

	}

	return result

}

func getAllSubmissionsID(submissionsList []interface{}) []string {

	var result []string

	for _, page := range submissionsList {

		submissionID := fmt.Sprintf("%d", int(page.(map[string]interface{})["id"].(float64)))

		result = append(result, submissionID)

	}

	return result

}

func getLatestHighestSubmissionsID(submissionList []interface{}) []string {

	var result []string
	var latestHighestSubmissionList = make(map[string]map[string]int64)

	for _, page := range submissionList {
		userJid := page.(map[string]interface{})["userJid"].(string)
		problemJid := page.(map[string]interface{})["problemJid"].(string)
		score := int64(page.(map[string]interface{})["latestGrading"].(map[string]interface{})["score"].(float64))
		submissionID := fmt.Sprintf("%d", int(page.(map[string]interface{})["id"].(float64)))

		_, foundUserJid := latestHighestSubmissionList[userJid]
		if !foundUserJid {
			latestHighestSubmissionList[userJid] = make(map[string]int64)
		}

		_, foundLaterSubmission := latestHighestSubmissionList[userJid][problemJid]
		if foundLaterSubmission {
			if latestHighestSubmissionList[userJid][problemJid] < score {
				latestHighestSubmissionList[userJid][problemJid] = score
				result = append(result, submissionID)
			}
		} else {
			latestHighestSubmissionList[userJid][problemJid] = score
			result = append(result, submissionID)
		}
	}

	for i := len(result)/2 - 1; i >= 0; i-- {
		opp := len(result) - 1 - i
		result[i], result[opp] = result[opp], result[i]
	}

	return result

}

func getSubmissionDetails(submissionID string, token string) map[string]interface{} {

	url := os.Getenv("URIEL_URL") + "/api/v2/contests/submissions/programming/id/" + submissionID

	bearer := "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	catchError(err)

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	catchError(err)

	body, err := ioutil.ReadAll(resp.Body)
	catchError(err)

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	return result

}

func processSubmission(submissionsID []string, token string) {

	os.Mkdir("submissions", 0770)

	for i := (len(submissionsID) - 1); i >= 0; i-- {

		submissionDetails := getSubmissionDetails(submissionsID[i], token)

		sourceCode := submissionDetails["data"].(map[string]interface{})["source"].(map[string]interface{})["submissionFiles"].(map[string]interface{})["source"].(map[string]interface{})["content"].(string)
		decodedSourceCode, err := b64.StdEncoding.DecodeString(sourceCode)
		catchError(err)

		problemAlias := submissionDetails["problemAlias"].(string)

		author := submissionDetails["profile"].(map[string]interface{})["username"].(string)

		submissionLanguage := submissionDetails["data"].(map[string]interface{})["submission"].(map[string]interface{})["gradingLanguage"]

		var fileFormat string
		if submissionLanguage == "Cpp11" {
			fileFormat = "cpp"
		} else if submissionLanguage == "C" {
			fileFormat = "c"
		} else if submissionLanguage == "Java" {
			fileFormat = "java"
		} else if submissionLanguage == "Python3" {
			fileFormat = "py"
		} else if submissionLanguage == "Pascal" {
			fileFormat = "pas"
		} else {
			fileFormat = "txt"
		}

		fileName := problemAlias + "_" + author + "." + fileFormat
		fmt.Println(fileName)

		f, err := os.Create("submissions/" + fileName)
		catchError(err)

		

		n, err := f.Write(decodedSourceCode)
		catchError(err)

		fmt.Printf("Wrote %d bytes\n", n)

		f.Sync()

		f.Close()
	}

}

func catchError(err error) {

	if err != nil {

		log.Fatalln(err)

	}

}
